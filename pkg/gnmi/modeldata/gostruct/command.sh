#!/bin/bash

# SPDX-FileCopyrightText: 2022 2020-present Open Networking Foundation <info@opennetworking.org>
#
# SPDX-License-Identifier: Apache-2.0

export PATH=$PATH:$(go env GOPATH)/bin

export GOPATH=$(go env GOPATH)

go get github.com/openconfig/ygot@v0.8.3
(cd $GOPATH/src/github.com/openconfig/ygot@v0.8.3 && go get -t -d ./...);
go get github.com/openconfig/public;
go install github.com/google/go-cmp/cmp;
go install github.com/openconfig/gnmi/ctree;
go install github.com/openconfig/gnmi/proto/gnmi;
go install github.com/openconfig/gnmi/value;
go get github.com/YangModels/yang;
go install github.com/golang/glog;
go install google.golang.org/protobuf/proto;
go install github.com/kylelemons/godebug/pretty;
go get github.com/openconfig/goyang/pkg/yang@v0.0.0-20200803193518-78bac27bdff1
go install google.golang.org/grpc;

go run $GOPATH/pkg/mod/github.com/openconfig/ygot@v0.8.3/generator/generator.go -output_file $HOME/Projects/gnmi1/fastAPI/gnxi-simulators/pkg/gnmi/modeldata/gostruct/generated.go -generate_fakeroot -package_name gostruct -exclude_modules ietf-interfaces -path github.com/openconfig/public,github.com/YangModels/yang $HOME/Projects/gnmi1/yang/gcl_customized/gcl.yang